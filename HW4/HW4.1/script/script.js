function toUpperCase(...arg) {
    let arrString = String(arg)
    return arrString.toUpperCase();

}

function map(fn, array) {
    let result = [];
    for ( let i = 0; i < array.length; i++) {
        result.push(fn(array[i]));
    }
    return console.log(result);
}

const myArray = ["word","excel","powerPoint","outlook","oneDrive"];
let msArray = map(toUpperCase, myArray);