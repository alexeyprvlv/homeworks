function Content () {
    this.head = "",
    this.body = "",
    this.footer = "",
    this.date = ""
};

Content.prototype.print = function(name) {
    document.write(`<header><h2>Заголовок ${name} - ${this.head}</h2></header>`)
    document.write(`<body><p>Тело ${name} - ${this.body}</p></body>`)
    document.write(`<footer><p>Футер ${name} - ${this.footer}</p></footer>`)
    document.write(`<div>Дата ${name} - ${this.date}</div>`)
}

Content.prototype.getData = function (name) {
    this.head = prompt(`Введите заголовок ${name}`);
    this.body = prompt(`Введите тело ${name}`);
    this.footer = prompt(`Введите футер ${name}`);
    this.date = new Date();
};


const myDocument = new Content;
myDocument.app = new Content;


myDocument.getData("документа");
myDocument.app.getData("приложения");
myDocument.print("документа");
myDocument.app.print("приложения");